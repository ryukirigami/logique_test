const { DataTypes } = require('sequelize');
const db = require('../config/db');

const User = db.define('User', {
  id: {
    type: DataTypes.INTEGER(),
    primaryKey: true,
    allowNull: false,
    autoIncrement: true,
  },
  name: {
    type: DataTypes.STRING(),
    allowNull: false,
  },
  address: {
    type: DataTypes.STRING(),
    allowNull: false,
  },
  email: {
    type: DataTypes.STRING(),
    allowNull: false,
    unique: true,
  },
  password: {
    type: DataTypes.STRING(),
    allowNull: false,
  },
  photos: {
    type: DataTypes.ARRAY(DataTypes.TEXT),
    allowNull: false,
  },
}, {
  tableName: 'user',
});

module.exports = User;
