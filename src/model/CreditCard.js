const { DataTypes } = require('sequelize');
const db = require('../config/db');
const User = require('./User');

const CreditCard = db.define('CreditCard', {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    allowNull: false,
    autoIncrement: true,
  },
  creditcard_type: {
    type: DataTypes.STRING(),
    allowNull: false,
  },
  creditcard_number: {
    type: DataTypes.STRING(),
    allowNull: false,
  },
  creditcard_name: {
    type: DataTypes.STRING(),
    allowNull: false,
  },
  creditcard_expired: {
    type: DataTypes.STRING(),
    allowNull: false,
  },
  creditcard_cvv: {
    type: DataTypes.STRING(),
    allowNull: false,
  },
}, {
  tableName: 'credit_card',
});

User.hasOne(CreditCard, { through: 'CreditCard', as: 'credit_card', foreignKey: { unique: true, name: 'user_id' } });
CreditCard.belongsTo(User, { through: 'CreditCard', as: 'user', foreignKey: 'user_id' });

module.exports = CreditCard;
