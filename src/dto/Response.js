class Response {
  constructor() {
    this.errors = [];
    this.data = null;
  }

  addError(error) {
    this.errors.push(error);
  }

  setErrors(errors) {
    this.errors = errors;
  }

  setData(data) {
    this.data = data;
  }
}

module.exports = Response;
