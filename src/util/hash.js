const bcrypt = require('bcrypt');

const salt = bcrypt.genSaltSync(10);

const hash = {
  compare: (password, hashedPassword) => bcrypt.compareSync(password, hashedPassword),
  encrypt: (password) => bcrypt.hashSync(password, salt),
};

module.exports = hash;
