const express = require('express');
const authentication = require('../middleware/authentication');

const api = express.Router();

const userRoute = require('./user/userRoute');

api.use('/user', authentication, userRoute);

module.exports = api;
