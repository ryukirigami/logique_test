const express = require('express');
const userController = require('./userController');
const validation = require('../../middleware/validation');
const { userSchema, userUpdateSchema } = require('./userSchema');

const route = express.Router();

route.get('/list', userController.getAll);
route.get('/:id', userController.getById);
route.post('/register', validation(userSchema), userController.add);
route.patch('/', validation(userUpdateSchema), userController.update);

module.exports = route;
