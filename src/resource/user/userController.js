const userService = require('./userService');
const ApiError = require('../../dto/ApiError');
const Response = require('../../dto/Response');

const userController = {

  getAll: (req, res, next) => userService.getAll(req)
    .then((result) => {
      if (result.errors) return next(ApiError.badRequest(result.errors));

      const response = new Response();
      response.setData(result);

      res.status(200).json(response);
    })
    .catch((err) => next(ApiError.serverError(err.message))),

  getById: (req, res, next) => userService.getById(req.params.id)
    .then((result) => {
      if (result.errors) return next(ApiError.notFound(result.errors));

      const response = new Response();
      response.setData(result);

      res.status(200).json(response);
    })
    .catch((err) => next(ApiError.serverError(err.message))),

  add: (req, res, next) => userService.add(req)
    .then((result) => {
      if (result.errors) return next(ApiError.badRequest(result.errors));

      const response = new Response();
      response.setData(result);

      res.status(200).json(response);
    })
    .catch((err) => next(ApiError.serverError(err.message))),

  update: (req, res, next) => userService.update(req)
    .then((result) => {
      if (result.errors) return next(ApiError.badRequest(result.errors));

      const response = new Response();
      response.setData(result);

      res.status(200).json(response);
    })
    .catch((err) => {
      next(ApiError.serverError(err.message));
    }),

};

module.exports = userController;
