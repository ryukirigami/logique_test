const { number } = require('yup');
const { object, string, array } = require('yup');

const userSchema = object({
  name: string().required(),
  address: string().required(),
  email: string().required().email(),
  password: string().required(),
  photos: array().required(),
  creditcard_type: string().required(),
  creditcard_number: string().required(),
  creditcard_name: string().required(),
  creditcard_expired: string().required(),
  creditcard_cvv: string().required(),
});

const userUpdateSchema = object({
  user_id: number().required(),
  email: string().email(),
});

module.exports = {
  userSchema,
  userUpdateSchema,
};
