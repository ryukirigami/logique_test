const User = require('../../model/User');
const CreditCard = require('../../model/CreditCard');
const db = require('../../config/db');
const hash = require('../../util/hash');

const defAttribute = [
  ['id', 'user_id'],
  'name', 'email', 'address', 'photos',
];

const defInclude = [
  {
    model: CreditCard,
    as: 'credit_card',
    attributes: {
      include: [
        ['creditcard_type', 'type'],
        ['creditcard_number', 'number'],
        ['creditcard_name', 'name'],
        ['creditcard_expired', 'expired'],
      ],
      exclude: [
        'id',
        'creditcard_type',
        'creditcard_number',
        'creditcard_name',
        'creditcard_expired',
        'creditcard_cvv',
        'user_id',
      ],
    },
  },
];

const getAll = async (req) => {
  const {
    q, lt, of, sb, ob,
  } = req.query;

  try {
    const users = await User.findAndCountAll({
      where: q ? { email: q } : {},
      limit: lt,
      offset: of,
      order: ob ? [[ob, sb || 'asc']] : null,
      attributes: defAttribute,
      include: defInclude,
    });

    return users;
  } catch (error) {
    return { errors: error };
  }
};

const getById = async (id) => {
  try {
    const user = await User.findByPk(id, {
      attributes: defAttribute,
      include: defInclude,
    });

    if (!user) throw Error('User not found.');

    return user;
  } catch (error) {
    return { errors: error };
  }
};

const add = async (req) => {
  const { body } = req;

  const transaction = await db.transaction();

  try {
    const user = await User.create({
      name: body.name,
      address: body.address,
      email: body.email,
      password: hash.encrypt(body.password),
      photos: body.photos,
    });

    await CreditCard.create({
      user_id: user.id,
      creditcard_type: body.creditcard_type,
      creditcard_number: body.creditcard_number,
      creditcard_name: body.creditcard_name,
      creditcard_expired: body.creditcard_expired,
      creditcard_cvv: body.creditcard_cvv,
    });

    await transaction.commit();

    return { user_id: user.id };
  } catch (error) {
    await transaction.rollback();
    console.log(error);
    return { errors: error };
  }
};

const update = async (req) => {
  const { body } = req;

  const transaction = await db.transaction();

  try {
    const user = await User.findByPk(body.user_id, { lock: true, transaction });

    if (!user) throw Error('user id does not found');

    if (body.password) body.password = hash.encrypt(body.password);

    await user.update(body, { transaction });

    await transaction.commit();
    return { success: true };
  } catch (error) {
    await transaction.rollback();
    return { errors: error };
  }
};

module.exports = {
  getAll,
  getById,
  add,
  update,
};
