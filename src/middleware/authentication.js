const ApiError = require('../dto/ApiError');

const authMiddleware = async (req, res, next) => {
  const authKey = req.headers.key;

  if (!authKey) return next(ApiError.notAuthorize('API key is missing.'));
  if (authKey === 'HiJhvL$T27@1u^%u86g') return next();
  return next(ApiError.notAuthorize('Invalid API key.'));
};

module.exports = authMiddleware;
