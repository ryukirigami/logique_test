const express = require('express');
const helmet = require('helmet');
const cors = require('cors');
const apis = require('./resource/routes');
const errorHandler = require('./error/errorHandler');

const app = express();

app.use(express.json());
app.use(cors());
app.use(helmet());

app.use('/', apis);

app.use(errorHandler);

module.exports = app;
