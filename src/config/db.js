const { Sequelize } = require('sequelize');

const db = new Sequelize(
  'postgres://postgres:password@localhost:5432/logique_db',
  {
    logging: false,
    sync: {
      alter: true,
    },
    pool: {
      max: 20,
      min: 0,
      idle: 5000,
      acquire: 5000,
    },
    define: {
      timestamps: false,
    },
  },
);

db.authenticate()
  .then(() => console.log('Database connected!'))
  .catch((err) => console.log(`Error: ${err}`));

module.exports = db;
