const ApiError = require('../dto/ApiError');
const Response = require('../dto/Response');

function apiErrorHandler(err, req, res, next) {
  const response = new Response();

  if (err instanceof ApiError) {
    if (err.message instanceof Array) {
      err.message.forEach((e) => {
        response.addError(e);
      });
    } else {
      response.addError(err.message);
    }

    return res.status(err.code).json(response);
  }

  response.addError('Something went wrong. Please try again later.');
  return next(res.status(500).json(response));
}

module.exports = apiErrorHandler;
