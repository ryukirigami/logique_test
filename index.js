const app = require('./src/app');
const db = require('./src/model');

db.sync();

app.listen(8080, () => {
  console.log('server up listening on port 8080');
});
