# prequisite:
```
    - node v16.11.0
    - db: postgress sql
```

## setup database
postgres://postgres:password@localhost:5432/logique_db

# install
```bash
    $ yarn install
```

# run
```bash
    $ yarn run dev # db schema generated from ORM 
```
